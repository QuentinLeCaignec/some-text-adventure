# Some Text Adventure

Simple interactive text adventure game in Typescript with Vue3+Vite and tick-knock ECS

## MVP TODO

- Setup multiple "rooms" with content (prose, interaction, items, etc.).
- Ability to navigate from room to room by interacting with the text/UI.
- Ability to interact with content of a room (look at, use, etc..)
- Ability to interact with items (pick up, look at, use, etc..)
- UI display for numerical values (health, gold, etc..)
- UI display for text values (player info, current room, etc..)

## Project Setup

```sh
npm install
```

### Compile and Hot-Reload for Development

```sh
npm run dev
```

### Type-Check, Compile and Minify for Production

```sh
npm run build
```

### Lint with [ESLint](https://eslint.org/)

```sh
npm run lint
```
