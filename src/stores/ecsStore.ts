import { defineStore } from "pinia";
import { Engine, Entity, Query, System } from "tick-knock";
import gameData from "@/ecs/game_data.json";
import { PLAYER } from "@/ecs/components/tags";
import { Room } from "@/ecs/components/room";
import { Prose } from "@/ecs/components/prose";
import { Item } from "@/ecs/components/item";
import { Location } from "@/ecs/components/location";
import { CurrentLocationSystem } from "@/ecs/systems/currentLocationSystem";
import { Health } from "@/ecs/components/health";
import { HealthSystem } from "@/ecs/systems/healthSystem";

interface IRoom {
  id: string,
  name: string,
  content: string,
}

interface IItem {
  name: string,
  locationID: string
}

export const useEcsStore = defineStore("ecs", {
  state: (): {
    engine: Engine,
    player: Entity,
    currentLocation: Entity | null,
  } => {
    return {
      engine: new Engine(),
      player: new Entity().add(PLAYER),
      currentLocation: null,
    };
  },
  getters: {
    getPlayer(): Entity {
      return this.player as Entity;
    },
    getCurrentLocation(): Entity {
      return this.currentLocation as Entity;
    },
    // entities(): Entity[] {
    //   return this.engine.entities as Entity[];
    // },
  },
  actions: {
    initializeEcs() {
      gameData.rooms.forEach((room: IRoom) => {
        // Rooms
        useEcsStore().addEntity(
          new Entity()
            .add(new Room(room.id, room.name))
            .add(new Prose(room.content)),
        );
      });
      // Items
      gameData.items.forEach((item: IItem) => {
        useEcsStore().addEntity(
          new Entity()
            .add(new Item(item.name))
            .add(new Location(item.locationID)),
        );
      });
      // Player
      useEcsStore().player
        .add(new Location(gameData.initialPlayerLocation))
        .add(new Health(100, 100));
      // Systems
      useEcsStore().addSystem(new CurrentLocationSystem());
      useEcsStore().addSystem(new HealthSystem());

      this.update();
    },
    // ECS
    update(dt: number = 0) {
      this.engine.update(dt);
      console.log("engine update");
    },
    addEntity(entity: Entity) {
      this.engine.addEntity(entity);
    },
    removeEntity(entity: Entity) {
      this.engine.removeEntity(entity);
    },
    addQuery(query: Query) {
      this.engine.addQuery(query);
    },
    removeQuery(query: Query) {
      this.engine.removeQuery(query);
    },
    addSystem(system: System) {
      this.engine.addSystem(system);
    },
    // Vue State
    setCurrentLocation(entity: Entity) {
      this.currentLocation = entity;
    },
  },
});
