import { defineStore } from "pinia";
import { markRaw } from "vue";
import type DefaultWidget from "@/modules/GameInterface/Widgets/DefaultWidget.vue";

export interface IWidget {
  type: DefaultWidget,
  data: number
}

export const useGeneralStore = defineStore("general", {
  state: (): {
    stuff: number,
    widgets: Array<IWidget>
  } => {
    return {
      stuff: 0,
      widgets: [],
    };
  },
  getters: {
    getStuff(): number {
      return this.stuff;
    },
    getWidgets(): Array<IWidget> {
      return this.widgets;
    },
  },
  actions: {
    addStuff(nb: number) {
      this.stuff += nb;
    },
    addWidget(type: DefaultWidget, data: number) {
      this.widgets.push({ type: markRaw(type), data });
    },
    resetWidgets() {
      this.widgets = [];
    },
  },
});
