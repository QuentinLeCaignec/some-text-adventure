export function getAssetUrl(path: string) {
  return new URL(`/src/assets/${path}`, import.meta.url).href;
}

export function getImageUrl(name: string) {
  return getAssetUrl(`images/${name}.png`);
}
