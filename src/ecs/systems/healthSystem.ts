import { Entity, IterativeSystem } from "tick-knock";
import { Health } from "@/ecs/components/health";
import { Damage } from "@/ecs/components/damage";

export class HealthSystem extends IterativeSystem {
  public constructor() {
    super((entity) => entity.hasAll(Damage, Health));
  }

  public updateEntity(entity: Entity) {
    // TODO: wtf, why do systems not add entities with the right components if those components are added later ???
    console.log(entity);
    const health = entity.get(Health)!;
    while (entity.has(Damage)) {
      const damage = entity.withdraw(Damage)!;
      health.value -= damage.value;
    }
  }
}
