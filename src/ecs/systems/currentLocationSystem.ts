import { Entity, EntitySnapshot, ReactionSystem } from "tick-knock";
import { useEcsStore } from "@/stores/ecsStore";
import { Room } from "@/ecs/components/room";
import { Location } from "@/ecs/components/location";

export class CurrentLocationSystem extends ReactionSystem {
  public constructor() {
    super((entity: Entity) => {
      return entity.hasAll(Room);
    });
  }

  public update(): void {
    const player = useEcsStore().player;
    for (const entity of this.entities) {
      if (entity.get(Room)!?.id === player.get(Location)!?.locationID) {
        useEcsStore().setCurrentLocation(entity);
        break;
      }
    }
  }

  // protected prepare(): void {
  //   for (const entity of this.entities) {
  //     this.entityAdded(entity); // TODO: how to convert entity into entity snapshot?
  //   }
  // }

  protected entityAdded = ({ current }: EntitySnapshot) => {
    console.log("entity added", current);
  };

  protected entityRemoved = ({ previous }: EntitySnapshot) => {
    console.log("entity removed", previous);
  };
}
