export class Health {
  public constructor(
    public value: number,
    public max: number,
  ) {
  }
}
