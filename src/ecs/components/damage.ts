import { LinkedComponent } from "tick-knock";

export class Damage extends LinkedComponent {
  public constructor(
    public readonly value: number,
  ) {
    super();
  }
}
